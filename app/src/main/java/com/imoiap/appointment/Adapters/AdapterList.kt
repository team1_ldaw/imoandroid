package com.imoiap.appointment.Adapters

import android.app.Application
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.imoiap.appointment.DatabaseActivity

import com.imoiap.appointment.SampleApplication
import com.imoiap.appointment.see_appointments
import android.app.Activity
import android.content.DialogInterface
//import javax.swing.text.StyleConstants.setIcon
import android.app.AlertDialog
import android.util.Log
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.imoiap.appointment.R
import com.koushikdutta.ion.Ion
import kotlinx.android.synthetic.main.activity_new_appointment.*
import org.json.JSONArray
import android.widget.TextView
import org.w3c.dom.Text


class AdapterList() : Filterable, RecyclerView.Adapter<AdapterList.ListHolder>(){
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    dataFitlered = data
                } else {
                    val filteredList: JsonArray? = JsonArray()
                    for (i in 0 until (data!!.size() - 1)) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match

                        val objectJson: JsonObject = data!!.get(i).asJsonObject
                        val date = objectJson.get("date").asString

                        if(date.contains(charString,true)){
                            filteredList?.add(objectJson)

                        }
                    }

                    dataFitlered = filteredList

                }

                val filterResults = FilterResults()
                filterResults.values = dataFitlered
                return filterResults
            }
            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                dataFitlered = filterResults.values as JsonArray?

                notifyDataSetChanged()
            }
        }
    }


    private var data: JsonArray?=null
    private var dataFitlered: JsonArray?=null
    private var context: Context?=null
    private var layoutResourceId:Int?=null
    private lateinit var app: SampleApplication
    private var activity: Activity? = null

    constructor(context: Context, layoutResourceId:Int, data: JsonArray, application: SampleApplication, activity: Activity):this(){
        this.data = data
        dataFitlered = data
        this.context = context
        this.layoutResourceId = layoutResourceId
        this.app = application
        this.activity = activity
        var emptyView = activity.findViewById<TextView>(com.imoiap.appointment.R.id.empty_view)
        if(data!!.size()==0){

            emptyView.setVisibility(View.VISIBLE);
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListHolder {
        val v = LayoutInflater.from(parent.context).inflate(layoutResourceId!!,parent,false)

        return ListHolder(v)

    }

    override fun getItemCount(): Int {
        return dataFitlered!!.size()
    }

    override fun onBindViewHolder(holder: ListHolder, position: Int) {
        //val objectJson: JsonObject = data!!.get(position).asJsonObject
        val objectJson: JsonObject = dataFitlered!!.get(position).asJsonObject
        val id = objectJson.get("idAppointment").asString

        var fechaConFormato = objectJson.get("date").asString
        var datos = fechaConFormato.split(" ")
        var fechaAux =datos[0].split("-")
        var hora = datos[1].split(":")
        var aux = " "
        var horaAux = " "
        when(fechaAux[1]){
            "1" -> aux = "Enero"
            "2" -> aux = "Febrero"
            "3" -> aux = "Marzo"
            "4" -> aux = "Abril"
            "5" -> aux = "Mayo"
            "6" -> aux = "Junio"
            "7" -> aux = "Julio"
            "8" -> aux = "Agosto"
            "9" -> aux = "Septiembre"
            "10" -> aux = "Octubre"
            "11" -> aux = "Noviembre"
            "12" -> aux = "Diciembre"
        }
        var fechaAux1 = fechaAux[2] + " de " + aux + " del " + fechaAux[0]
        if(hora[0].toInt() >= 12){
            if(hora[0].toInt() == 12){
                horaAux =fechaAux1 + " a las " + hora[0].toString()+':'+hora[1]+" PM"
            }
            else{
                var horaAux1 = hora[0].toInt() - 12
                horaAux =fechaAux1 + " a las " + horaAux1.toString()+':'+hora[1]+" PM"
            }

        }
        else{
            horaAux =fechaAux1 + " a las " + hora[0]+':'+hora[1]+" AM"
        }



        holder.idText.text = horaAux!!
        holder.nameText.text = objectJson.get("idAppointment").asString
        holder.usernameText.text = objectJson.get("date").asString
        holder.webpageText.text = objectJson.get("date").asString
        holder.delete.setOnClickListener{
            AlertDialog.Builder(context)
                .setTitle("IMO")
                .setMessage("Estas seguro de que quieres cancelar la cita?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes,
                    DialogInterface.OnClickListener { dialog, whichButton ->
                        Toast.makeText(
                            context,
                            "Success",
                            Toast.LENGTH_SHORT
                        ).show()
                        DatabaseActivity(context,1, 2, app).execute(id)
                        //Toast.makeText(context,id,Toast.LENGTH_SHORT).show()
                        context?.startActivity(Intent(context,see_appointments::class.java))
                        activity?.finish()
                    })
                .setNegativeButton(android.R.string.no, null).show()

        }

        var dataAux: JsonArray
        var linearLayoutManager: LinearLayoutManager
        var adapter: AdapterListNew

        var dateAux = objectJson.get("date").asString
        var parts = dateAux.split(" ")
        dateAux = parts[0]

        holder.reschedule.setOnClickListener{
            val input: RecyclerView = RecyclerView(context!!)
            input.adapter

            Ion.with(context)
                .load("https://imocitas.000webhostapp.com/api/checkAvailable.php")
                .setBodyParameter("date", dateAux)
                .setBodyParameter("service", objectJson.get("idService_id").asString)
                .asJsonArray()
                .setCallback { error, result ->
                    if(error == null){
                        Log.i("Salida", result.toString())

                        dataAux = result
                        linearLayoutManager = LinearLayoutManager(context)
                        input.layoutManager = linearLayoutManager
                        adapter = AdapterListNew(context!!,R.layout.item_list_new,dataAux,app,this.activity!!,true,id)
                        input.adapter = adapter



                    }
                }

            AlertDialog.Builder(context)
                .setTitle("Cambiar hora")
                .setMessage("Escoje una nueva hora")
                .setView(input)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setNegativeButton(android.R.string.no, null).show()


        }

        when(objectJson.get("idService_id").asString){
            "1" ->  {
            holder.image.setImageResource(R.drawable.protesisocular)
            holder.idText2.text = "Prótesis ocular"
            }
            "2" ->  {
                holder.image.setImageResource(R.drawable.bajavision)
                holder.idText2.text = "Baja visión"
            }
            "3" ->  {
                holder.image.setImageResource(R.drawable.imagenologia)
                holder.idText2.text = "Imagenología"
            }
            "4" ->  {
                holder.image.setImageResource(R.drawable.retina)
                holder.idText2.text = "Retina"
            }
            "5" ->  {
                holder.image.setImageResource(R.drawable.segmentoanteriorycornea)
                holder.idText2.text = "Segmento anterior & córnea"
            }
            "6" ->  {
                holder.image.setImageResource(R.drawable.glaucoma)
                holder.idText2.text = "Glaucoma"
            }
            "7" ->  {
                holder.image.setImageResource(R.drawable.oftalmopediatria)
                holder.idText2.text = "Oftalmopediatria"
            }

            else -> {
                holder.image.setImageResource(R.drawable.consultabasica)
                holder.idText2.text = "Error"
            }


        }
        //objectJson.addProperty("fecha",horaAux!!)

        //data!![position] = objectJson
        //dataFitlered!![position] = objectJson
    }

    class ListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var idText: TextView
        internal var idText2: TextView
        internal var nameText: TextView
        internal var usernameText: TextView
        internal var webpageText: TextView
        internal var delete: Button
        internal var reschedule: Button
        internal var image: ImageView

        init{
            idText = itemView.findViewById(R.id.id_text) as TextView
            idText2 = itemView.findViewById(R.id.id_text2) as TextView
            nameText = itemView.findViewById(R.id.name_text) as TextView
            usernameText = itemView.findViewById(R.id.username_text) as TextView
            webpageText = itemView.findViewById(R.id.webpage_text) as TextView
            delete = itemView.findViewById(R.id.delete) as Button
            reschedule = itemView.findViewById(R.id.reschedule) as Button
            image = itemView.findViewById(R.id.tipo_servicio) as ImageView
        }
    }




}