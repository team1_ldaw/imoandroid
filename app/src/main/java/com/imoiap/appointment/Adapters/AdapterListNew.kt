package com.imoiap.appointment.Adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.icu.util.Calendar
import android.os.Build
import android.os.Handler
import android.provider.CalendarContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.imoiap.appointment.*
import com.koushikdutta.ion.Ion

class AdapterListNew(): RecyclerView.Adapter<AdapterListNew.ListHolder>() {



    private var data: JsonArray?=null
    private var context: Context?=null
    private var layoutResourceId:Int?=null
    private lateinit var app: SampleApplication
    private var activity: Activity? = null
    private var startMillis: Long = 5
    private var reschedule: Boolean = true
    private var id: String?=null
    private var empty: Boolean = true

    constructor(context: Context, layoutResourceId:Int, data: JsonArray, application: SampleApplication, activity: Activity, startMillis: Long):this(){
        this.data = data
        this.context = context
        this.layoutResourceId = layoutResourceId
        this.app = application
        this.activity = activity
        this.startMillis = startMillis
        this.reschedule = false
        this.id = "0"
        var emptyView = activity.findViewById<TextView>(com.imoiap.appointment.R.id.empty_view)
        if(data!!.size()==0){
            emptyView.setVisibility(View.VISIBLE);

        }
        if(data!!.size()==0){
            this.empty = true
            this.data!!.add("{empty}")
        }
        else this.empty = false

    }

    constructor(context: Context, layoutResourceId:Int, data: JsonArray, application: SampleApplication, activity: Activity,reschedule: Boolean,id: String):this(){
        this.data = data
        this.context = context
        this.layoutResourceId = layoutResourceId
        this.app = application
        this.activity = activity
        this.reschedule = reschedule
        this.id = id
        var emptyView = activity.findViewById<TextView>(com.imoiap.appointment.R.id.empty_view)
        if(data!!.size()==0){
            emptyView.setVisibility(View.VISIBLE);

        }
        if(data!!.size()==0){
            this.empty= true
            this.data!!.add("{empty}")
        }
        else this.empty = false
    }


    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListHolder {
        val v = LayoutInflater.from(parent.context).inflate(layoutResourceId!!,parent,false)

        return ListHolder(v)

    }

    override fun getItemCount(): Int {
        return data!!.size()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(holder: AdapterListNew.ListHolder, position: Int) {
        if(empty){
            holder.idText.text = "No hay citas disponibles"
            holder.idText2.text = "Disponibles: 0 "
        }
        else{
            var objectJson: JsonObject = data!!.get(position).asJsonObject
            //val objectJson: JsonObject = dataFitlered!!.get(position).asJsonObject
            //val id = objectJson.get("idAppointment").asString

            var fechaConFormato = objectJson.get("fecha").asString
            var datos = fechaConFormato.split(" ")
            var fechaAux =datos[0].split("-")
            var hora = datos[1].split(":")
            var aux = " "
            var horaAux = " "
            when(fechaAux[1]){
                "1" -> aux = "Enero"
                "2" -> aux = "Febrero"
                "3" -> aux = "Marzo"
                "4" -> aux = "Abril"
                "5" -> aux = "Mayo"
                "6" -> aux = "Junio"
                "7" -> aux = "Julio"
                "8" -> aux = "Agosto"
                "9" -> aux = "Septiembre"
                "10" -> aux = "Octubre"
                "11" -> aux = "Noviembre"
                "12" -> aux = "Diciembre"
            }
            var fechaAux1 = fechaAux[2] + " de " + aux + " del " + fechaAux[0]
            if(hora[0].toInt() == 12){
                var horaAux1 = hora[0].toInt()
                horaAux =fechaAux1 + " a las " + horaAux1.toString()+':'+hora[1]+" PM"
            }
            else if(hora[0].toInt() > 12){
                var horaAux1 = hora[0].toInt() - 12
                horaAux =fechaAux1 + " a las " + horaAux1.toString()+':'+hora[1]+" PM"
            }
            else{
                horaAux =fechaAux1 + " a las " + hora[0]+':'+hora[1]+" AM"
            }


            holder.idText.text = horaAux!!

            //objectJson.addProperty("fecha",horaAux!!)

            //data!![position] = objectJson
           //     get(position).asJsonObject.addProperty(position.toString(),objectJson.toString())

            holder.idText2.text = "Disponibles: " + objectJson.get("disponibles").asString
            holder.confirm.setOnClickListener{
                AlertDialog.Builder(context)
                    .setTitle("IMO")
                    .setMessage("Confirmar")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes,
                        DialogInterface.OnClickListener { dialog, whichButton ->
                            if(reschedule){
                                DatabaseActivity(context,1, 2, app).execute(id)
                            }
                            //Toast.makeText(context,objectJson.get("fecha").asString,Toast.LENGTH_LONG).show()
                            Ion.with(context)
                                .load("https://imocitas.000webhostapp.com/api/createappointment.php")
                                .setBodyParameter("carnet", app.carnet)
                                .setBodyParameter("date", objectJson.get("fecha").asString)
                                .setBodyParameter("service", objectJson.get("servicio").asString)
                                .asJsonArray()
                                .setCallback { error, result ->
                                    if(error == null){
                                        Log.i("Salida", result.toString())

                                        //data = result
                                        Toast.makeText(
                                            context,
                                            "test" + result.toString(),
                                            Toast.LENGTH_SHORT
                                        ).show()

                                    }
                                }



                            var date = objectJson.get("fecha").asString.split(' ')
                            var dateT = date[0]
                            var timeT = date[1]

                            var builder = NotificationCompat.Builder(context!!, "1")
                                .setSmallIcon(R.drawable.com_facebook_button_icon)
                                .setContentTitle("Cita")
                                .setContentText(dateT)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                            newAppointment(dateT,timeT)


                            //agregar notificacion

                            var dates = dateT.split("-")
                            var times = timeT.split(":")

                            //var now = Date()
                            //var then d= Date(dates[0].toInt(),dates[1].toInt(),dates[2].toInt(),times[0].toInt()
                            // ,times[1].toInt(),times[2].toInt())
                            var now = System.currentTimeMillis()
                            var then = Calendar.getInstance()
                            Log.println(Log.INFO,"Error",now.toString() + " " + then.toString())
                            //Toast.makeText(context,now.toString() + " " + then.toString(),Toast.LENGTH_LONG).show()
                            then.set(dates[0].toInt(),dates[1].toInt()-1,dates[2].toInt(),times[0].toInt(),
                                times[1].toInt(),times[2].toInt())



                            var mills = (then.timeInMillis - now) - 1000 * 60 * 60
                            //Toast.makeText(this,mills.toString(),Toast.LENGTH_LONG).show()
                            Log.println(Log.INFO,"mills, now",now.toString())
                            Log.println(Log.INFO,"mills, then",then.timeInMillis.toString())
                            Log.println(Log.INFO,"mills",mills.toString())
                            Handler().postDelayed(Runnable {
                                with(NotificationManagerCompat.from(context!!)) {
                                    // notificationId is a unique int for each notification that you must define
                                    notify(1, builder.build())
                                }
                            },mills)

                            //Toast.makeText(context,id,Toast.LENGTH_SHORT).show()
                            context?.startActivity(Intent(context, see_appointments::class.java))
                            (context as new_appointment).calendario(startMillis)

                            activity?.finish()
                        })
                    .setNegativeButton(android.R.string.no, null).show()

            }
        }

    }

    class ListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var idText: TextView
        internal var idText2: TextView
        internal var confirm: Button

        init{
            idText = itemView.findViewById(R.id.Fecha) as TextView
            idText2 = itemView.findViewById(R.id.disponibles) as TextView
            confirm = itemView.findViewById(R.id.boton) as Button
        }
    }

    @SuppressLint("MissingPermission")
    fun newAppointment(date: String, time: String){
        var dateTime = date.plus(" ").plus(time)
        //Toast.makeText(this,"get",Toast.LENGTH_SHORT)
        //var test = DatabaseActivity(this,1, 0, app).execute(app.carnet, dateTime, 1)


        //Log.i("Salida", "none")
        val startMillis: Long = Calendar.getInstance().run {
            set(2019, 10, 18, 23, 30)
            timeInMillis
        }
        val endMillis: Long = Calendar.getInstance().run {
            set(2019, 10, 18, 23, 45)
            timeInMillis
        }

        val intent = Intent(Intent.ACTION_INSERT)
            .setData(CalendarContract.Events.CONTENT_URI)
            .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
            .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
            .putExtra(CalendarContract.Events.TITLE, "Cita en IMO")
            .putExtra(
                CalendarContract.Events.DESCRIPTION,
                "Asistir al Instituto Mexicano de Oftalmologia")
            .putExtra(CalendarContract.Events.EVENT_LOCATION, "IMO IAP")

// get the event ID that is the last element in the Uri

    }

    /*override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        // use position to know the selected item
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {

    }*/

}