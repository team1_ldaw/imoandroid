package com.imoiap.appointment

import android.content.Context
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    lateinit var ACTIVITY: bottomNavTest

    override fun onAttach(context: Context) {
        super.onAttach(context)
        ACTIVITY = context as bottomNavTest
    }
}