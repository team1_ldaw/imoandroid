package com.imoiap.appointment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Optional;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

public class DatabaseActivity  extends AsyncTask{
    private Context context;
    private int byGetOrPost = 0;
    private int action = 0;
    private SampleApplication app;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    private String result = " ";

    //flag 0 means get and 1 means post.(By default it is get.)
    public DatabaseActivity(Context context,int flag,int action, SampleApplication app) {
        this.context = context;
        this.action = action;
        byGetOrPost = flag;
        result = " ";
        this.app = app;
    }


    protected void onPreExecute(){
        //Toast.makeText(context,"Test",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if(app.queryResult && action==0){ //crear cita
            Toast.makeText(context,"Success",Toast.LENGTH_SHORT).show();
        }
        else if(action==1){ //Ver mis citas
            String res = (String)o;
            res.substring(1);
            res = res.substring(0, res.length() - 1);
            String[] arrOfRes = res.split(",",0);
            for (String a : arrOfRes){
                a.substring(1);
                a = a.substring(0, a.length() - 1);
                String[] arrOfA = a.split(":",0);
                app.list.add(arrOfA);
                //System.out.println(a);
            }

        }
        else if(action==2){
            Toast.makeText(context,o.toString(),Toast.LENGTH_SHORT).show();

        }

        else Toast.makeText(context,"Error",Toast.LENGTH_SHORT).show();
        //Toast.makeText(context,o.toString(),Toast.LENGTH_SHORT).show();
        Toast.makeText(context,"Success",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected String doInBackground(Object[] objects){
        if(byGetOrPost == 0){ //means by Get Method
            if(action == 1){
                try{
                    String user = (String)objects[0];
                    String link = "https://imocitas.000webhostapp.com/service.php";

                    URL url = new URL(link);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(link));
                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(response.getEntity().getContent()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    in.close();
                    result = sb.toString();
                    return sb.toString();
                } catch(Exception e){
                    return new String("Exception: " + e.getMessage());
                }
            }

        } else{
            if(action==0){
                try{
                    app.queryResult = false;
                    String user = (String)objects[0];
                    String dateTime = (String)objects[1];
                    String servicio = (String)objects[2];

                    String link="https://imocitas.000webhostapp.com/api/createappointment.php";
                    String data  = URLEncoder.encode("iduser", "UTF-8") + "=" +
                            URLEncoder.encode(user, "UTF-8");
                    data += "&" + URLEncoder.encode("date", "UTF-8") + "=" +
                            URLEncoder.encode(dateTime, "UTF-8");
                    data += "&" + URLEncoder.encode("service", "UTF-8") + "=" +
                            URLEncoder.encode(servicio, "UTF-8");

                    URL url = new URL(link);
                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                    wr.write( data );
                    wr.flush();

                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));

                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    // Read Server Response
                    while((line = reader.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    app.queryResult = true;
                    return sb.toString();


                } catch(Exception e){
                    return new String("Exception: " + e.getMessage());
                }
            }
            else if(action == 2){
                try{
                    app.queryResult = false;
                    String id = (String)objects[0];

                    String link="https://imocitas.000webhostapp.com/delete_service.php";
                    String data  = URLEncoder.encode("idAppointment", "UTF-8") + "=" +
                            URLEncoder.encode(id, "UTF-8");


                    URL url = new URL(link);
                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                    wr.write( data );
                    wr.flush();

                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));

                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    // Read Server Response
                    while((line = reader.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    app.queryResult = true;
                    return sb.toString();


                } catch(Exception e){
                    return new String("Exception: " + e.getMessage());
                }
            }
            else if(action == 2){
                try{
                    app.queryResult = false;
                    String id = (String)objects[0];

                    String link="https://imocitas.000webhostapp.com/delete_service.php";
                    String data  = URLEncoder.encode("idAppointment", "UTF-8") + "=" +
                            URLEncoder.encode(id, "UTF-8");


                    URL url = new URL(link);
                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                    wr.write( data );
                    wr.flush();

                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));

                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    // Read Server Response
                    while((line = reader.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    app.queryResult = true;
                    return sb.toString();


                } catch(Exception e){
                    return new String("Exception: " + e.getMessage());
                }
            }

        }
        return "Tst";
    }



    /*//@Override
    protected void onPostExecute(){
        //this.statusField.setText("Login Successful");
        //Toast.makeText(this,"Success",Toast.LENGTH_LONG);
        //this.roleField.setText(result);
        Toast.makeText(context,""+app.queryResult,Toast.LENGTH_SHORT).show();
    }*/
}