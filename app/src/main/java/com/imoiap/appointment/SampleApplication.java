package com.imoiap.appointment;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.util.ArrayList;

public class SampleApplication extends Application {

    public boolean queryResult;
    public ArrayList<Object> list;
    public String carnet;

    public void onCreate(){
        super.onCreate();
        carnet = new String();
        queryResult = false;
        list = new ArrayList<Object>();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }
}