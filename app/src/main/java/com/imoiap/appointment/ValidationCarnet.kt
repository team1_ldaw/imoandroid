package com.imoiap.appointment

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.isVisible
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.koushikdutta.ion.Ion
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_validation_carnet.*

class ValidationCarnet : AppCompatActivity() {

    private lateinit var app : SampleApplication
    private lateinit var db : FirebaseFirestore 


    private lateinit var data: JsonArray
    private lateinit var json: JsonObject

    private var context: Context = this
    private val SHARED_PREFERENCES = "MI_APP"
    private val PRIVATE_MODE = 0

    companion object {
        private const val TAG = "KotlinActivity"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_validation_carnet)
        db = FirebaseFirestore.getInstance()
        app = application as SampleApplication
        if (supportActionBar != null)
            supportActionBar?.hide()

        //Elementos graficos de interfaz
        val carnet = findViewById<EditText>(R.id.carnetField)
        val btnValidateCarnet = findViewById<Button>(R.id.btn_validateLogin)
        val carnet_progressBar = findViewById<ProgressBar>(R.id.carnet_progressBar)

        val btnuserManual = findViewById<Button>(R.id.btn_userManual)
        //val textTest = findViewById<TextView>(R.id.testText)

        //Funcion pa hacerlo funcionar
        btnValidateCarnet.setOnClickListener {

            //validateCarnet()
            app.carnet = carnet.text.toString();

            //textTest.setText("hola")

            if(carnet.text.isEmpty()){
                Toast.makeText(applicationContext, "Por favor ingrese un carnet", Toast.LENGTH_SHORT).show()
                carnetField.requestFocus()
            }else{
                carnet_progressBar.visibility = View.VISIBLE
                btnValidateCarnet.isEnabled = false
                iniitalizeData()
            }



            //startActivity(Intent(this, LoginActivity::class.java))


        }

        btnuserManual.setOnClickListener {
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse("https://docs.google.com/document/d/14N-vqUDhgk_yDewsL8lR58CmgPpZu542W5OB-4n4vbc/edit?usp=sharing")
            startActivity(openURL)
        }



    }

    fun iniitalizeData(){
        //Toast.makeText(applicationContext, "Conexión Correcta", Toast.LENGTH_SHORT).show()

        Ion.with(context)
            .load("https://imocitas.000webhostapp.com/carnets.php")
            .setBodyParameter("carnet", app.carnet)
            .asString()
            .setCallback { e, result ->
                if(e == null){
                    if(result.equals("[]")){
                        Toast.makeText(applicationContext, "Carnet no encontrado en la base de datos", Toast.LENGTH_SHORT).show()
                        carnet_progressBar.visibility = View.GONE
                        btn_validateLogin.isEnabled = true

                    }else{
                        Log.i("Salida", result)
                        carnet_progressBar.visibility = View.GONE
                        btn_validateLogin.isEnabled = true
                        //testText.setText(result)
                        startActivity(Intent(this, LoginActivity::class.java))
                    }
                }

            }


    }


}



