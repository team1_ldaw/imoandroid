package com.imoiap.appointment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import org.w3c.dom.Text

class change_email : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_email)
        auth = FirebaseAuth.getInstance()
        val email = findViewById<EditText>(R.id.email)
        val confirmButton = findViewById<Button>(R.id.button)
        val confirm = findViewById<CheckBox>(R.id.confirm)
        val verificar = findViewById<Button>(R.id.verificar)

        if(auth.currentUser!!.isEmailVerified){
            verificar.visibility = View.GONE
        }

        verificar.setOnClickListener{
            auth.currentUser!!.sendEmailVerification()
        }

        confirmButton.setOnClickListener{
            if(email.text.isNotBlank()){
                if(confirm.isChecked){
                    val user = auth.currentUser
                    user?.updateEmail(email.text.toString())
                        ?.addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                //Log.d(TAG, "User email address updated.")
                                Toast.makeText(applicationContext, "Correo actualizado exitosamente", Toast.LENGTH_SHORT).show()
                            }
                            else Toast.makeText(applicationContext, "Error", Toast.LENGTH_SHORT).show()
                        }
                }
            }
            else Toast.makeText(applicationContext, "Porfavor ingrese una direccion de correo", Toast.LENGTH_SHORT).show()
        }

    }
}
