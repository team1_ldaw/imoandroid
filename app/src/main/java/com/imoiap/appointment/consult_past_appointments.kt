package com.imoiap.appointment

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.JsonArray
import com.imoiap.appointment.Adapters.AdapterList
import com.koushikdutta.ion.Ion
import kotlinx.android.synthetic.main.activity_see_appointments.*
import android.graphics.pdf.PdfDocument
import android.net.Uri
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.view.View
import android.util.DisplayMetrics
import android.view.WindowManager
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.widget.*
import java.io.File


class consult_past_appointments: AppCompatActivity() {

    private lateinit var app: SampleApplication
    private lateinit var auth: FirebaseAuth
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var data: JsonArray
    private lateinit var adapter: AdapterList
    private lateinit var bitmap: Bitmap


    private var context: Context = this
    private val SHARED_PREFERENCES = "MI_APP"
    private val PRIVATE_MODE = 0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_consult_past_appointments)
        auth = FirebaseAuth.getInstance();
        app = application as SampleApplication
        val search = findViewById<SearchView>(R.id.search2)
        val btn_imprimir = findViewById<Button>(R.id.btn_imprimir)
        val llScroll = findViewById<ScrollView>(R.id.llScroll)

        //var task = DatabaseActivity(this,0, 1, app).execute(auth.currentUser?.uid)
        iniitalizeData()

        btn_imprimir.setOnClickListener {
            export_to_pdf()
        }





        search.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                adapter.filter.filter(p0)
                adapter.notifyDataSetChanged()
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                adapter.filter.filter(p0)
                return false
            }

        })
    }


    fun iniitalizeData(){
        Ion.with(context)
            .load("https://imocitas.000webhostapp.com/api/citasPreviasCorrespondientesACarnet.php")
            .setBodyParameter("carnet", app.carnet)
            .asJsonArray()
            .setCallback { error, result ->
                if(error == null){
                    Log.i("Salida", result.toString())

                    data = result
                    initializeList()

                }
            }
    }

    fun initializeList(){
        linearLayoutManager = LinearLayoutManager(context)
        recycler_view.layoutManager = linearLayoutManager
        adapter = AdapterList(context,R.layout.item_list2,data,app,this)
        recycler_view.adapter = adapter
    }

    fun export_to_pdf(){
        Ion.with(context)
            .load("https://imocitas.000webhostapp.com/pdf/generate_pdf.php")
            .setBodyParameter("carnet", app.carnet)
            .asString()
            .setCallback { error, result ->
                if(error == null){
                    Log.i("Salida", result)
                    downloadPdf()

                }
            }
    }

    fun downloadPdf(){
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse("https://imocitas.000webhostapp.com/pdf/MisCitas.pdf")
        startActivity(openURL)
    }




}
