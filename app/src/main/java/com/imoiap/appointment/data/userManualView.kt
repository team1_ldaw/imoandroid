package com.imoiap.appointment.data

import android.os.Bundle
import android.webkit.WebView
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.imoiap.appointment.R

import kotlinx.android.synthetic.main.activity_user_manual_view.*

class userManualView : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_manual_view)
        setSupportActionBar(toolbar)

        val userManual = findViewById<WebView>(R.id.usr_manual_webview)

        userManual.webViewClient
        userManual.loadUrl("https://docs.google.com/document/d/14N-vqUDhgk_yDewsL8lR58CmgPpZu542W5OB-4n4vbc/edit")

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

}
