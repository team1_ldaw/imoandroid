package com.imoiap.appointment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.app.DownloadManager
import android.app.ProgressDialog
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.widget.ProgressBar
import android.widget.Toast
import com.koushikdutta.ion.Ion
import java.io.File
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import com.imoiap.appointment.data.userManualView
import com.koushikdutta.async.future.FutureCallback
import com.koushikdutta.ion.ProgressCallback


class middleview : AppCompatActivity() {
    private var context: Context = this
    private lateinit var progressBar: ProgressBar
    private lateinit var progressDialog: ProgressDialog
    private lateinit var progressCallback: ProgressCallback
    var file = File("https://drive.google.com/file/d/0BwhL8nFHqznvaHFWUjBINXRuNE0/view?usp=sharing")


    //var f = File()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_middleview)



        if (supportActionBar != null)
            supportActionBar?.hide()

        val btnLoginView = findViewById<Button>(R.id.btnLoginView)
        val btnRegister = findViewById<Button>(R.id.btnRegister)
        val btn_userManual = findViewById<TextView>(R.id.btn_userManual)

        btnLoginView.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        btnRegister.setOnClickListener {
            startActivity(Intent(this, registerActivity::class.java))
        }

        btn_userManual.setOnClickListener{
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse("https://docs.google.com/document/d/14N-vqUDhgk_yDewsL8lR58CmgPpZu542W5OB-4n4vbc/edit?usp=sharing")
            startActivity(openURL)
        }
    }


}
