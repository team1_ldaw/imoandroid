package com.imoiap.appointment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.Time
import android.view.View
import android.R.attr.password
import android.os.AsyncTask.execute
//import org.junit.runner.Request.method
//import androidx.core.app.ComponentActivity
//import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.R.attr.password
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Intent
import android.os.AsyncTask.execute
//import org.junit.runner.Request.method

import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.icu.util.Calendar
import android.net.Uri
import android.os.Build
import android.provider.CalendarContract
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.auth.FirebaseAuth
import androidx.core.os.HandlerCompat.postDelayed
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Handler
import android.util.Log
import androidx.core.os.postDelayed
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

import android.app.DatePickerDialog
import android.content.Context
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonArray
import com.google.protobuf.LazyStringArrayList
import com.imoiap.appointment.Adapters.AdapterList
import com.imoiap.appointment.Adapters.AdapterListNew
import com.koushikdutta.ion.Ion
import kotlinx.android.synthetic.main.activity_new_appointment.*
import kotlinx.android.synthetic.main.activity_new_appointment.carnet_progressBar
import kotlinx.android.synthetic.main.activity_validation_carnet.*


class new_appointment : AppCompatActivity(),AdapterView.OnItemSelectedListener{
    override fun onNothingSelected(p0: AdapterView<*>?) {
        selected = "none"
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        var p = p2 + 1
        if(p==10) p++
        selected = p.toString()
    }

    private lateinit var auth: FirebaseAuth
    private lateinit var app: SampleApplication
    var options = arrayOf("Prótesis ocular", "Baja visión",
        "Imagenología","Retina","Segmento anterior & córnea","Glaucoma","Oftalmopediatria")
    var selected: String = "none"
    private var context: Context = this
    private lateinit var data: JsonArray
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: AdapterListNew


    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_appointment)
        auth = FirebaseAuth.getInstance();
        val carnet_progressBar = findViewById<ProgressBar>(R.id.carnet_progressBar)
        app = application as SampleApplication
        var date = findViewById<EditText>(R.id.date)
        var time = findViewById<EditText>(R.id.time)
        var confirmar = findViewById<Button>(R.id.confirmar)
        var datePicker = findViewById<Button>(R.id.btn_datePicker)
        var yeartext = findViewById<TextView>(R.id.test1)
        var monthtext = findViewById<TextView>(R.id.test2)
        var daytext = findViewById<TextView>(R.id.test3)
        var checked = findViewById<CheckBox>(R.id.checkBox1)

        //recycler view


        //Dropdown
        var spinner = findViewById<Spinner>(R.id.spinner)
        spinner!!.setOnItemSelectedListener(this)
        var adapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
            android.R.layout.simple_spinner_item, options)

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner!!.setAdapter(adapter)
        //spinner.setOnItemSelectedListener(this);

        //Datepicker
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        datePicker.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{ view, mYear,
                                                                                 mMonth, mDay ->
                datePicker.setText("" + mYear + "-" + (mMonth+1) + "-" + mDay)
                yeartext.setText("" + mYear)
                monthtext.setText("" + mMonth)
                daytext.setText("" + mDay)

            }, year, month, day)

            dpd.show()
        }


        confirmar.setOnClickListener{
            carnet_progressBar.visibility = View.VISIBLE
            val year2 = test1.text.toString().toInt()
            val month2 = test2.text.toString().toInt()
            val day2 = test3.text.toString().toInt()


            val startMillis: Long = Calendar.getInstance().run {
                set(year2, month2, day2, 0, 0)
                timeInMillis
            }
            val endMillis: Long = Calendar.getInstance().run {
                set(year2, month2, day2, 0, 0)
                timeInMillis
            }


            iniitalizeData(datePicker.text.toString(), startMillis)
        }

        /*public void loginPost(View view){
            String username = usernameField.getText().toString();
            String password = passwordField.getText().toString();
            method.setText("Get Method")
            SigninActivity(this, status, role, 0).execute(username, password)
            Toast.makeText(this,"Post",Toast.LENGTH_SHORT)
            SigninActivity(this, status, role, 1).execute(username, password)
        }*/

    }


    fun calendario(startMillis: Long){
        if(checkBox1.isChecked){
            val intent = Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
                .putExtra(CalendarContract.Events.TITLE, "Cita en IMO")
                .putExtra(CalendarContract.Events.DESCRIPTION, "Asistir al Instituto Mexicano de Oftalmologia")
                .putExtra(CalendarContract.Events.EVENT_LOCATION, "IMO IAP")
            startActivity(intent)
        }
    }


    fun iniitalizeData(date: String, startMillis: Long){
        Ion.with(context)
            .load("https://imocitas.000webhostapp.com/api/checkAvailable.php")
            .progressBar(ProgressBar(context))
            .setBodyParameter("date", date)
            .setBodyParameter("service", selected)
            .asJsonArray()
            .setCallback { error, result ->
                if(error == null){
                    Log.i("Salida", result.toString())
                    carnet_progressBar.visibility = View.GONE
                    data = result
                    initializeList(startMillis)

                }
            }
    }

    fun initializeList(startMillis: Long){
        linearLayoutManager = LinearLayoutManager(context)
        recycler_view2.layoutManager = linearLayoutManager
        adapter = AdapterListNew(context,R.layout.item_list_new,data,app,this, startMillis)
        recycler_view2.adapter = adapter

    }



}
