package com.imoiap.appointment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
//import androidx.test.orchestrator.junit.BundleJUnitUtils.getResult
import java.nio.file.Files.size
//import org.junit.experimental.results.ResultMatchers.isSuccessful
import androidx.annotation.NonNull
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task


class password_change_activity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_change_activity)
        val email = findViewById<EditText>(R.id.text)
        val button = findViewById<Button>(R.id.button)
        auth = FirebaseAuth.getInstance()



        button.setOnClickListener{
            if(email.text.isNotBlank()){
                auth.sendPasswordResetEmail(email.text.toString())
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(applicationContext, "success, email with password change sent", Toast.LENGTH_SHORT).show()
                        }
                        else{
                            Toast.makeText(applicationContext, "Error, invalid login", Toast.LENGTH_SHORT).show()
                        }
                    }

            }
            else Toast.makeText(applicationContext, "Porfavor ingrese una direccion de correo", Toast.LENGTH_SHORT).show()
        }



    }
}
