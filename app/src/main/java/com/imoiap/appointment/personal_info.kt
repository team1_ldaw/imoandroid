package com.imoiap.appointment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.google.firebase.auth.FirebaseAuth


class personal_info : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private lateinit var app : SampleApplication
    private lateinit var email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_info)

        app = application as SampleApplication
        auth = FirebaseAuth.getInstance();


        val carne = findViewById<TextView>(R.id.CarnetPon)
        val mail = findViewById<TextView>(R.id.correoPon)

        carne.setText(app.carnet)

        val user = auth.currentUser


        if (user != null) {
            email = user.email.toString()
            mail.setText(email)
        }
    }




}
