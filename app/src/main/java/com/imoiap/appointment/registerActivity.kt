package com.imoiap.appointment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_middleview.*
import kotlinx.android.synthetic.main.activity_register.*

class registerActivity : AppCompatActivity() {
    // [START declare_auth]
    private lateinit var auth: FirebaseAuth
    // [END declare_auth]

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        // [START initialize_auth]
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
        // [END initialize_auth]

        // Buttons
        val btnregisterActivity = findViewById<Button>(R.id.btnRegisterButon)

        //Texts
        val email = findViewById<EditText>(R.id.regmail)
        val password = findViewById<EditText>(R.id.regpassword)
        val name = findViewById<EditText>(R.id.regname)
        val lastname = findViewById<EditText>(R.id.reglastname)

        btnregisterActivity.setOnClickListener {
            createAccount(email.text.toString(), password.text.toString(), name.text.toString(), lastname.text.toString())
        }
    }

    private fun createAccount(email: String, password: String, firstname : String, lastname : String) {
        //Log.d(TAG, "createAccount:$email")
        val uid : String

        if(regname.text.toString().isEmpty()){
            Toast.makeText(applicationContext, "Por favor ingrese su nombre", Toast.LENGTH_SHORT).show()
            regname.requestFocus()
            return
        }

        if(reglastname.text.toString().isEmpty()){
            Toast.makeText(applicationContext, "Por favor ingrese sus apellidos", Toast.LENGTH_SHORT).show()
            reglastname.requestFocus()
            return
        }

        if(regmail.text.toString().isEmpty()){
            Toast.makeText(applicationContext, "Por favor ingrese su correo electrónico", Toast.LENGTH_SHORT).show()
            regmail.requestFocus()
            return
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(regmail.text.toString()).matches()){
            Toast.makeText(applicationContext, "Correo electrónico inválido", Toast.LENGTH_SHORT).show()
            regmail.requestFocus()
            return
        }

        if(regpassword.text.toString().isEmpty() || regpassword.text.toString().length < 8){
            Toast.makeText(applicationContext, "Ingrese una contraseña de mínimo 8 carácteres", Toast.LENGTH_SHORT).show()
            regpassword.requestFocus()
            return
        }


        auth.createUserWithEmailAndPassword(regmail.text.toString(), regpassword.text.toString())
            .addOnCompleteListener(this) {task ->
                if(task.isSuccessful){
                    val currUser = FirebaseAuth.getInstance().currentUser
                    val uid = currUser!!.uid

                    startActivity(Intent(this,LoginActivity::class.java))
                    Toast.makeText(applicationContext, "Cuenta creada exitosamente", Toast.LENGTH_SHORT).show()
                    finish()
                } else {
                    Toast.makeText(baseContext, "Correo ya existente u otro error.",
                        Toast.LENGTH_SHORT).show()
                }

                    /*val userMap = HashMap<String, String>()
                    userMap["firstname"] = firstname
                    userMap["lastname"] = lastname

                    val uidDatabase = FirebaseDatabase.getInstance().getReference("users").child(uid)

                    uidDatabase.setValue(userMap).addOnCompleteListener { task ->
                        if(task.isSuccessful){
                            startActivity(Intent(this,LoginActivity::class.java))
                            Toast.makeText(applicationContext, "Registro exitoso", Toast.LENGTH_SHORT).show()
                            finish()
                        }else{
                            Toast.makeText(baseContext, "Error en el inicio de sesión. Registro.",
                                Toast.LENGTH_SHORT).show()
                        }
                    }*/





                    //val user : users(name : String, lastname : String, uid : String)

                    /*FirebaseDatabase.getInstance().getReference("users")
                        .child(FirebaseAuth.getInstance().currentUser.uid)
                        .setValue(user).addOnCompleteListener {
                            if(task.isSuccessful){
                                startActivity(Intent(this,LoginActivity::class.java))
                                Toast.makeText(applicationContext, "Registro exitoso", Toast.LENGTH_SHORT).show()
                                finish()
                            }else{
                                Toast.makeText(baseContext, "Error en el inicio de sesión. Registro.",
                                    Toast.LENGTH_SHORT).show()
                            }
                        }
                }else{
                    Toast.makeText(baseContext, "Sign Up failed. Try again after some time.",
                        Toast.LENGTH_SHORT).show()
                }*/




            }


    }

}
