package com.imoiap.appointment

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.JsonArray
import com.imoiap.appointment.Adapters.AdapterList
import com.koushikdutta.ion.Ion
import kotlinx.android.synthetic.main.activity_see_appointments.*
import java.util.*

class see_appointments : AppCompatActivity() {

    private lateinit var app: SampleApplication
    private lateinit var auth: FirebaseAuth
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var data: JsonArray
    private lateinit var adapter: AdapterList

    private var context: Context = this
    private val SHARED_PREFERENCES = "MI_APP"
    private val PRIVATE_MODE = 0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_see_appointments)
        auth = FirebaseAuth.getInstance();
        app = application as SampleApplication
        val search = findViewById<SearchView>(R.id.search)

        //var task = DatabaseActivity(this,0, 1, app).execute(auth.currentUser?.uid)
        iniitalizeData()
        search.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                adapter.filter.filter(p0)
                adapter.notifyDataSetChanged()
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                adapter.filter.filter(p0)
                return false
            }

        })
    }

    fun iniitalizeData(){
        Ion.with(context)
            .load("https://imocitas.000webhostapp.com/api/citasCorrespondientesACarnet.php")
            .setBodyParameter("carnet", app.carnet)
            .asJsonArray()
            .setCallback { error, result ->
                if(error == null){
                    Log.i("Salida", result.toString())

                    data = result
                    initializeList()

                }
            }
    }

    fun initializeList(){
        linearLayoutManager = LinearLayoutManager(context)
        recycler_view.layoutManager = linearLayoutManager
        adapter = AdapterList(context,R.layout.item_list2,data,app,this)
        recycler_view.adapter = adapter

    }




}
