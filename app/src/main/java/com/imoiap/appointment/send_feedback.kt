package com.imoiap.appointment

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.koushikdutta.ion.Ion

class send_feedback : AppCompatActivity() {

    private var context: Context = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_feedback)
        var title = findViewById<EditText>(R.id.title)
        var content = findViewById<EditText>(R.id.content)
        var confirmar = findViewById<Button>(R.id.confirmar)
        confirmar.setOnClickListener {

            if(title.text.isNotBlank() && content.text.isNotBlank()){
                //Toast.makeText(context,"prueba",Toast.LENGTH_SHORT).show()
                Ion.with(context)
                    .load("https://imocitas.000webhostapp.com/feedback.php")
                    .setBodyParameter("title", title.text.toString())
                    .setBodyParameter("content", content.text.toString())
                    .setBodyParameter("plataform","android")
                    .asJsonArray()
                    .setCallback { error, result ->
                        if(error == null){
                            Log.i("Salida", result.toString())


                        }
                    }
            }
            Toast.makeText(context,"Gracias",Toast.LENGTH_SHORT).show()
            finish()
        }
    }


}
