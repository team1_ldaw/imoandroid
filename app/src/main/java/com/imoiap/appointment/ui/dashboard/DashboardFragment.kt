package com.imoiap.appointment.ui.dashboard

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.imoiap.appointment.*

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        val textView: TextView = root.findViewById(R.id.text_dashboard)
        dashboardViewModel.text.observe(this, Observer {
            textView.text = it
        })

        //Test for controlling the app
        val goImo = root.findViewById<Button>(R.id.goToIMO)
        val institucionBtn = root.findViewById<Button>(R.id.institucion)
        val contactoBtn = root.findViewById<Button>(R.id.contacto)
        val staffBtn = root.findViewById<Button>(R.id.btn_staff)
        val faq = root.findViewById<Button>(R.id.btn_faq)


        //Ir al IMO
        goImo.setOnClickListener {

            val gmmIntentUri = Uri.parse("google.navigation:q=20.5756529,-100.3653847")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }

        institucionBtn.setOnClickListener{
            startActivity(Intent(activity, consult_institution::class.java))
        }

        contactoBtn.setOnClickListener{
            startActivity(Intent(activity, consult_contact::class.java))
        }

        staffBtn.setOnClickListener {
            startActivity(Intent(activity, staffView::class.java))
        }

        faq.setOnClickListener {
            startActivity(Intent(activity, faq_activity::class.java))
        }


        return root
    }
}