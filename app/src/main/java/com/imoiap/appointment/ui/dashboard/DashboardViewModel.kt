package com.imoiap.appointment.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DashboardViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Todo lo relacionado al IMO :D"
    }
    val text: LiveData<String> = _text
}