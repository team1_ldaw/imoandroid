package com.imoiap.appointment.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.imoiap.appointment.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val textView: TextView = root.findViewById(R.id.text_home)
        homeViewModel.text.observe(this, Observer {
            textView.text = it
        })

        //Test for controlling the app
        val testButton = root.findViewById<Button>(R.id.citas)
        val previousApp = root.findViewById<Button>(R.id.buttonPA)
        val newApp = root.findViewById<FloatingActionButton>(R.id.floatingActionButton2)


        testButton.setOnClickListener {
            startActivity(Intent(activity, see_appointments::class.java))
        }

        previousApp.setOnClickListener{
            startActivity(Intent(activity, consult_past_appointments::class.java))
        }

        newApp.setOnClickListener{
            startActivity(Intent(activity, new_appointment::class.java))
        }

        return root
    }

}