package com.imoiap.appointment.ui.notifications

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.imoiap.appointment.*

class NotificationsFragment : BaseFragment() {

    private lateinit var notificationsViewModel: NotificationsViewModel
    private lateinit var auth: FirebaseAuth
    private lateinit var app: SampleApplication

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
            ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_notifications, container, false)
        val textView: TextView = root.findViewById(R.id.text_notifications)
        notificationsViewModel.text.observe(this, Observer {
            textView.text = it
        })

        //app = application as SampleApplication

        auth = FirebaseAuth.getInstance()

        val incomingCarnet = ACTIVITY.carnete

        
        val change = root.findViewById<Button>(R.id.button2)
        val recover = root.findViewById<Button>(R.id.button3)
        val verify = root.findViewById<Button>(R.id.verificar)
        val web = root.findViewById<Button>(R.id.webAccount)
        val pi = root.findViewById<Button>(R.id.pi)
        val logout = root.findViewById<Button>(R.id.buttonLogOut)
        val ppolitics = root.findViewById<Button>(R.id.btn_ppolitics)

        val uriWeb = "https://imocita.herokuapp.com/register/?car=" + incomingCarnet

        change.setOnClickListener{
            startActivity(Intent(activity, change_email::class.java))
        }

        recover.setOnClickListener{
            startActivity(Intent(activity, password_change_activity::class.java))
        }

        pi.setOnClickListener{
            startActivity(Intent(activity, personal_info::class.java))
        }

        web.setOnClickListener{
            val uris = Uri.parse(uriWeb)
            val intents = Intent(Intent.ACTION_VIEW, uris)
            startActivity(intents)
        }


        //Verficar Correo
        if(auth.currentUser!!.isEmailVerified){
            verify.visibility = View.GONE
        }

        verify.setOnClickListener{
            auth.currentUser!!.sendEmailVerification()
        }

        //Cerrar sesion
        logout.setOnClickListener {
            auth.signOut()
            startActivity(Intent(activity, LoginActivity::class.java))
            activity!!.finish()
        }


        ppolitics.setOnClickListener {
            startActivity(Intent(activity, politicsActivity::class.java))
        }

        return root
    }
}