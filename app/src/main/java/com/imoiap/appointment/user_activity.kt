package com.imoiap.appointment

//import ShakeDetector
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId

import kotlinx.android.synthetic.main.activity_user_activity.*
import ShakeDetector.OnShakeListener
import ShakeDetector
import android.content.Context
import android.content.Context.SENSOR_SERVICE
import androidx.core.content.ContextCompat.getSystemService
//import androidx.core.app.ComponentActivity
//import androidx.core.app.ComponentActivity.ExtraData
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T





class user_activity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var mSensorManager: SensorManager
    private lateinit var mAccelerometer: Sensor
    private lateinit var mShakeDetector: ShakeDetector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_activity)
        auth = FirebaseAuth.getInstance();
        val logout = findViewById<Button>(R.id.buttonLogOut)
        val recover = findViewById<Button>(R.id.button3)
        val change = findViewById<Button>(R.id.button2)
        val newApp = findViewById<Button>(R.id.buttonNC)
        val citas = findViewById<Button>(R.id.citas)
        val previousApp = findViewById<Button>(R.id.buttonPA)
        val btngotoIMO = findViewById<Button>(R.id.goToIMO)
        val contacto = findViewById<Button>(R.id.contacto)
        val institucion = findViewById<Button>(R.id.institucion)
        val btn_staff = findViewById<Button>(R.id.btn_staff)
        val btn_account_settings = findViewById<Button>(R.id.btn_account_settings)




        // ShakeDetector initialization
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mAccelerometer = mSensorManager
            .getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mShakeDetector = ShakeDetector()
        mShakeDetector.setOnShakeListener { count ->
            /*
                         * The following method, "handleShakeEvent(count):" is a stub //
                         * method you would use to setup whatever you want done once the
                         * device has been shook.
                         */
            startActivity(Intent(this, send_feedback::class.java))
        }

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("token", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                // Log and toast
                //val msg = getString(R.string.msg_token_fmt, token)
                Log.d("token", token)
                //Toast.makeText(baseContext, token, Toast.LENGTH_SHORT).show()
            })


        contacto.setOnClickListener{
            startActivity(Intent(this, consult_contact::class.java))
        }

        institucion.setOnClickListener{
            startActivity(Intent(this, consult_institution::class.java))
        }

        citas.setOnClickListener{
            startActivity(Intent(this, see_appointments::class.java))
        }

        previousApp.setOnClickListener{
            startActivity(Intent(this,consult_past_appointments::class.java))
        }

        newApp.setOnClickListener{
            startActivity(Intent(this, new_appointment::class.java))
        }

        change.setOnClickListener{
            startActivity(Intent(this, change_email::class.java))
        }

        logout.setOnClickListener {
            auth.signOut()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
        recover.setOnClickListener{
            startActivity(Intent(this, password_change_activity::class.java))
        }

        btn_staff.setOnClickListener {
            startActivity(Intent(this, staffView::class.java))
        }

        btngotoIMO.setOnClickListener {

            val gmmIntentUri = Uri.parse("google.navigation:q=20.5756529,-100.3653847")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }

        btn_account_settings.setOnClickListener {
            startActivity(Intent(this, bottomNavTest::class.java))
        }
    }

    public override fun onResume() {
        super.onResume()
        // Add the following line to register the Session Manager Listener onResume
        mSensorManager.registerListener(
            mShakeDetector,
            mAccelerometer,
            SensorManager.SENSOR_DELAY_UI
        )
    }

    public override fun onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector)
        super.onPause()
    }

}
